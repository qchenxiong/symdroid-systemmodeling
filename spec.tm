<TeXmacs|1.0.7>

<style|article>

<\body>
  <doc-data|<doc-title|System Modeling of
  SymDroid>|<\doc-author-data|<author-name|Chenxiong Qian>>
    \;
  </doc-author-data>>

  <section|The classes need to be modeled from scratch>

  \;

  To support the execution of dalvik bytecode, our system must implement some
  import system classes: <with|font-series|bold|Object>,
  <with|font-series|bold|Class>, <with|font-series|bold|String>,
  <with|font-series|bold|StringBuilder>, <with|font-series|bold|Integer>,
  <with|font-series|bold|Intent>, <with|font-series|bold|Bundle>. We only
  model these classes at first, we plan to model others in future work.

  \;

  <section|The classes from JVM>\ 

  \;

  Because of the complex relations between Android System classes, it is
  impossible to model all the classes related to sytem classes. For example,
  the classes related to String includes:
  <with|font-shape|italic|java.nio.charset.Charset,
  java.nio.charset.Charsets, java.util.Locale>, etc. These classes are all
  important and complex, it will be a heavy work to model them from scratch.
  So, for these classes, we implements the same functions with the help of
  classes from JVM.

  We should keep a table to record the mapping relations of the classes:

  <big-table|<block|<tformat|<twith|table-hmode|auto>|<table|<row|<cell|java.lang.String>|<cell|symdroid.systemmodel.lang.SDString>>|<row|<cell|java.lang.Object>|<cell|symdroid.systemmodel.lang.SDObject>>|<row|<cell|java.lang.Class>|<cell|symdroid.systemmodel.lang.SDClass>>|<row|<cell|java.io.Serializable
  (I)>|<cell|symdroid.systemmodel.io.SDSerializable>>|<row|<cell|java.lang.Comparable
  (I)>|<cell|symdroid.systemmodel.lang.SDComparable>>|<row|<cell|java.lang.CharSequence
  (I)>|<cell|symdroid.systemmodel.lang.SDCharsequence>>|<row|<cell|java.lang.StringBuilder>|<cell|java.lang.systemmodel.lang.SDStringBuilder>>|<row|<cell|java.lang.Appendable
  (I)>|<cell|java.lang.systemmodel.lang.SDAppendable>>|<row|<cell|java.lang.Integer>|<cell|java.lang.systemmodel.lang.SDInteger>>|<row|<cell|*.lang.reflect.AnnotationElement
  (I)>|<cell|*.lang.reflect.SDAnnotationElement>>|<row|<cell|*.lang.reflect.Annotation
  (I)>|<cell|*.lang.reflect.SDAnnotation>>|<row|<cell|*.lang.reflect.GenericDeclaration
  (I)>|<cell|*.lang.reflect.SDGenericDeclaration>>|<row|<cell|*.lang.reflect.Type
  (I)>|<cell|*.lang.reflect.SDType>>|<row|<cell|*.lang.reflect.TypeVariable
  (I)>|<cell|*.lang.reflect.SDTypeVariable>>>>>|>

  \;

  <section|Naming problem>

  \;

  There are two problems about naming: names of the classes need modeling and
  names of the methods of those classes. Because the JVM has the classes with
  the same name of those classes, we need to distinguish them for the reason
  that we may use the class of JVM to implement the function of our model
  class with the same name, or every class inherit the class
  <with|font-series|bold|Object>. On the other hand, for some special methods
  such as <with|font-series|bold|toString<with|font-shape|italic|>()>, we
  cannot use the same name because that the return type of
  <with|font-series|bold|toString()<with|font-shape|italic|>> is
  <with|font-series|bold|String>. So, we use the ``SD+classname'' to replace
  the origin name, like: SDString, SDInteger, SDIntent, etc. For methods, we
  replace the ``toString'' with ``toSDString'', etc.

  We should keep a table to recording the mapping relations of the method
  names:

  <big-table|<block*|<tformat|<table|<row|<cell|SDObject>|<cell|toString()>|<cell|toSDString()>>|<row|<cell|>|<cell|getClass()>|<cell|getSDClass()>>|<row|<cell|>|<cell|notify()>|<cell|SDNotify()>>|<row|<cell|>|<cell|notifyAll()>|<cell|SDNotifyAll()>>|<row|<cell|>|<cell|wait()>|<cell|SDWait()>>|<row|<cell|>|<cell|>|<cell|>>>>>|>

  \;

  <section|Native methods>

  \;

  For the methods, the JPF(Java Path Finder) use a mechanism called MJI which
  implements the native methods in other mapping classes. For example, there
  is a class named <with|font-series|bold|JPF_java_lang_Class>, which has
  many static methods implements the related native methods of the origin
  class <with|font-series|bold|Class>.

  Considering the MJI is complex, so we just leave the native methods
  umimplemented now. The detail of the implementation need discussion.

  \;

  <section|Exception>

  \;

  By now, we just ignore all the exceptions.

  \;
</body>

<\initial>
  <\collection>
    <associate|language|british>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|1|1>>
    <associate|auto-4|<tuple|3|1>>
    <associate|auto-5|<tuple|2|1>>
    <associate|auto-6|<tuple|4|1>>
    <associate|auto-7|<tuple|5|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|table>
      <tuple|normal||<pageref|auto-3>>

      <tuple|normal||<pageref|auto-5>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>The
      classes need to be modeled from scratch>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>The
      classes from JVM> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Naming
      problem> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Native
      methods> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>Exception>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>