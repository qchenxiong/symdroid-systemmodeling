package symdroid.systemmodel;


/**
 * @date 2012-10-15
 * @author vincent
 */
public class Main {
    
    public static void main(String[] args){
        System.out.println(Integer.highestOneBit(65));
        System.out.println(Integer.lowestOneBit(64));
    }
}
