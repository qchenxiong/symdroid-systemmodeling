package symdroid.systemmodel.lang;

/**
 * @date 2012-10-17
 * @author vincent
 */
public interface SDAppendable {

    SDAppendable append(char c);
    
    SDAppendable append(SDCharSequence cs);
    
    SDAppendable append(SDCharSequence cs, int start, int end);
}
