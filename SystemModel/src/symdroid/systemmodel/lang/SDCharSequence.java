package symdroid.systemmodel.lang;

/**
 * @date 2012-10-15
 * @author vincent
 */
public interface SDCharSequence {
    
    public int length();
    
    public char charAt(int index);
    
    public SDCharSequence subSequence(int start, int end);
    
    public SDString toSDString();
}
