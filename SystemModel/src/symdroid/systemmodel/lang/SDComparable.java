package symdroid.systemmodel.lang;

/**
 * @date 2012-10-15
 * @author vincent
 */
public interface SDComparable<T> {

    int compareTo(T another);
}
