package symdroid.systemmodel.lang.reflect;

import symdroid.systemmodel.lang.SDString;

/**
 * @date 2012-10-18
 * @author vincent
 */
public interface SDTypeVariable<D extends SDGenericDeclaration> extends SDType {

    SDType[] getBounds();
    
    D getGenericDeclaration();
    
    SDString getName();
}
