package symdroid.systemmodel.lang.reflect;

/**
 * @date 2012-10-18
 * @author vincent
 */
public interface SDGenericDeclaration {

    SDTypeVariable<?>[] getTypeParameters();
    
}
