package symdroid.systemmodel.lang.reflect;

import symdroid.systemmodel.lang.SDClass;
import symdroid.systemmodel.lang.SDObject;
import symdroid.systemmodel.lang.SDString;

/**
 * @date 2012-10-18
 * @author vincent
 */
public interface SDAnnotation {

    SDClass<? extends SDAnnotation> annotationType();
    
    boolean equals(SDObject obj);
    
    int hashCode();
    
    SDString toSDString();
}
