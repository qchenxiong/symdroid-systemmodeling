package symdroid.systemmodel.lang.reflect;

import symdroid.systemmodel.lang.SDClass;

/**
 * @date 2012-10-18
 * @author vincent
 */
public interface SDAnnotatedElement {

    <T extends SDAnnotation> T getAnnotation(SDClass<T> annotationType);
    
    SDAnnotation[] getAnnotations();
    
    SDAnnotation[] getDeclaredAnnotations();
    
    boolean isAnnotationPresent(SDClass<? extends SDAnnotation> annotationType);
}
