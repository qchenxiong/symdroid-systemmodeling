package symdroid.systemmodel.lang;

import symdroid.systemmodel.io.SDSerializable;

/**
 * @date 2012-10-15
 * @author vincent
 */
public final class SDStringBuilder extends SDObject implements SDAppendable, 
        SDCharSequence, SDSerializable{
    
    public StringBuilder SDStringBuilderProxy;
    
    public SDStringBuilder(){
        SDStringBuilderProxy = new StringBuilder();
    }
    
    public SDStringBuilder(int capacity){
        SDStringBuilderProxy = new StringBuilder(capacity);
    }
    
    public SDStringBuilder(SDCharSequence cs){
        SDStringBuilderProxy = new StringBuilder(cs.toSDString().SDStringProxy);
    }
    
    public SDStringBuilder(SDString str){
        SDStringBuilderProxy = new StringBuilder(str.SDStringProxy);
    }
    
    public SDStringBuilder append(boolean b){
        SDStringBuilderProxy.append(b);
        return this;
    }
    
    @Override
    public SDStringBuilder append(char c){
        SDStringBuilderProxy.append(c);
        return this;
    }
    
    public SDStringBuilder append(int i){
        SDStringBuilderProxy.append(i);
        return this;
    }
    
    public SDStringBuilder append(long l){
        SDStringBuilderProxy.append(l);
        return this;
    }
    
    public SDStringBuilder append(float f){
        SDStringBuilderProxy.append(f);
        return this;
    }
    
    public SDStringBuilder append(double d){
        SDStringBuilderProxy.append(d);
        return this;
    }
    
    public SDStringBuilder append(SDObject object){
        return append(object.toSDString());
    }
    
    public SDStringBuilder append(SDString str){
        if(str != null){
            SDStringBuilderProxy.append(str.SDStringProxy);
        }else{
            SDStringBuilderProxy.append("");
        }
        return this;
    }
    
    public SDStringBuilder append(char[] chars){
        SDStringBuilderProxy.append(chars);
        return this;
    }
    
    public SDStringBuilder append(char[] str, int offset, int len){
        SDStringBuilderProxy.append(str, offset, len);
        return this;
    }

    @Override
    public SDStringBuilder append(SDCharSequence cs) {
        if(cs != null){
            SDStringBuilderProxy.append(cs.toSDString().SDStringProxy);
        }else{
            SDStringBuilderProxy.append("");
        }
        return this;
    }

    @Override
    public SDStringBuilder append(SDCharSequence cs, int start, int end) {
        if(cs != null){
            SDStringBuilderProxy.append(cs.toSDString().SDStringProxy, start, end);
        }else{
            SDStringBuilderProxy.append("");
        }
        return this;
    }
    
    public SDStringBuilder appendCodePoint(int codePoint){
        SDStringBuilderProxy.appendCodePoint(codePoint);
        return this;
    }
    
    public SDStringBuilder delete(int start, int end){
        SDStringBuilderProxy.delete(start, end);
        return this;
    }
    
    public SDStringBuilder deleteCharAt(int index){
        SDStringBuilderProxy.deleteCharAt(index);
        return this;
    }
    
    public SDStringBuilder insert(int offset, boolean b){
        SDStringBuilderProxy.insert(offset, b);
        return this;
    }
    
    public SDStringBuilder insert(int offset, char c){
        SDStringBuilderProxy.insert(offset,c);
        return this;
    }
    
    public SDStringBuilder insert(int offset, int i){
        SDStringBuilderProxy.insert(offset, i);
        return this;
    }
    
    public SDStringBuilder insert(int offset, long l){
        SDStringBuilderProxy.insert(offset, l);
        return this;
    }
    
    public SDStringBuilder insert(int offset, float f){
        SDStringBuilderProxy.insert(offset, f);
        return this;
    }
    
    public SDStringBuilder insert(int offset, double d){
        SDStringBuilderProxy.insert(offset, d);
        return this;
    }
    
    public SDStringBuilder insert(int offset, SDObject object){
        if(object != null){
            SDStringBuilderProxy.insert(offset, object.toSDString().SDStringProxy);
        }else{
            SDStringBuilderProxy.insert(offset,"");
        }
        return this;
    }
    
    public SDStringBuilder insert(int offset, SDString str){
        SDStringBuilderProxy.insert(offset, str.SDStringProxy);
        return this;
    }
    
    public SDStringBuilder insert(int offset, char[] ch){
        SDStringBuilderProxy.insert(offset,ch);
        return this;
    }
    
    public SDStringBuilder insert(int offset, char[] str, int strOffset, int strLen){
        SDStringBuilderProxy.insert(offset, str, strOffset, strLen);
        return this;
    }
    
    public SDStringBuilder insert(int offset, SDCharSequence cs){
        if(cs != null){
            SDStringBuilderProxy.insert(offset, cs.toSDString().SDStringProxy);
        }else{
            SDStringBuilderProxy.insert(offset, "");
        }
        return this;
    }
    
    public SDStringBuilder insert(int offset, SDCharSequence cs, int start, int end){
        if(cs != null){
            SDStringBuilderProxy.insert(offset,cs.toSDString().SDStringProxy,start,end);
        }else{
            SDStringBuilderProxy.insert(offset,"",start,end);
        }
        return this;
    }
    
    public SDStringBuilder replace(int start, int end, SDString str){
        SDStringBuilderProxy.replace(start, end, str.SDStringProxy);
        return this;
    }
    
    public SDStringBuilder reverse(){
        SDStringBuilderProxy.reverse();
        return this;
    }

    @Override
    public int length() {
        return SDStringBuilderProxy.length();
    }

    @Override
    public char charAt(int index) {
        return SDStringBuilderProxy.charAt(index);
    }

    @Override
    public SDCharSequence subSequence(int start, int end) {
        char[] tmp = SDStringBuilderProxy.subSequence(start, end).toString().toCharArray();
        return new SDString(tmp);
    }

    @Override
    public SDString toSDString() {
        char[] tmp = SDStringBuilderProxy.toString().toCharArray();
        return new SDString(tmp);
    }
}
