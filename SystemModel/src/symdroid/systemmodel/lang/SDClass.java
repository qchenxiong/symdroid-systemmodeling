package symdroid.systemmodel.lang;

import symdroid.systemmodel.io.SDSerializable;
import symdroid.systemmodel.lang.reflect.SDAnnotatedElement;
import symdroid.systemmodel.lang.reflect.SDAnnotation;
import symdroid.systemmodel.lang.reflect.SDGenericDeclaration;
import symdroid.systemmodel.lang.reflect.SDType;
import symdroid.systemmodel.lang.reflect.SDTypeVariable;

/**
 * @date 2012-10-15
 * @author vincent
 */
public final class SDClass<T> implements SDSerializable, SDAnnotatedElement, SDGenericDeclaration, SDType{
    
    private transient SDString name;
    
    private SDClass(){
    }
    
    

    @Override
    public <T extends SDAnnotation> T getAnnotation(SDClass<T> annotationType) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public SDAnnotation[] getAnnotations() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public SDAnnotation[] getDeclaredAnnotations() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isAnnotationPresent(SDClass<? extends SDAnnotation> annotationType) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public SDTypeVariable<?>[] getTypeParameters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
