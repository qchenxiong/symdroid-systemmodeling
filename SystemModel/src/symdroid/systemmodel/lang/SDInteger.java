package symdroid.systemmodel.lang;

/**
 * @date 2012-10-17
 * @author vincent
 */
public class SDInteger extends SDObject implements SDComparable<SDInteger> {
    
    public Integer SDIntegerProxy;
    
    private SDInteger(Integer integer){
        this.SDIntegerProxy = integer;
    }
    
    public SDInteger(int value){
        SDIntegerProxy = new Integer(value);
    }
    
    public SDInteger(SDString str){
        SDIntegerProxy = new Integer(str.SDStringProxy);
    }
    
    public byte byteValue(){
        return SDIntegerProxy.byteValue();
    }

    @Override
    public int compareTo(SDInteger another) {
        return SDIntegerProxy.compareTo(another.SDIntegerProxy);
    }
    
    public static int compare(int lhs, int rhs){
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }
    
    public static SDInteger decode(SDString str){
        Integer tmp = Integer.decode(str.SDStringProxy);
        return new SDInteger(tmp);
    }
    
    public double doubleValue(){
        return SDIntegerProxy.doubleValue();
    }
    
    public boolean equals(SDObject o){
        return (o instanceof SDInteger) && this.compareTo((SDInteger)o) == 0;
    }
    
    public float floatValue(){
        return SDIntegerProxy.floatValue();
    }
    
    public static SDInteger getInteger(SDString str){
        return new SDInteger(Integer.getInteger(str.SDStringProxy));
    }
    
    public static SDInteger getInteger(SDString str,  int defaultValue){
        return new SDInteger(Integer.getInteger(str.SDStringProxy, defaultValue));
    }
    
    public static SDInteger getInteger(SDString str, SDInteger defaultValue){
        return new SDInteger(Integer.getInteger(str.SDStringProxy, defaultValue.SDIntegerProxy));
    }
    
    public int hashCode(){
        return SDIntegerProxy.hashCode();
    }
    
    public int intValue(){
        return SDIntegerProxy.intValue();
    }
    
    public long longValue(){
        return SDIntegerProxy.longValue();
    }
    
    public static int parseInt(SDString string){
       return Integer.parseInt(string.SDStringProxy);
    }
    
    public static int parseInt(SDString string, int radix){
        return Integer.parseInt(string.SDStringProxy, radix);
    }

    public short shortValue(){
        return SDIntegerProxy.shortValue();
    }
    
    public static SDString toBinaryString(int i){
        return new SDString(Integer.toBinaryString(i).toCharArray());
    }
    
    public static SDString toHexString(int i){
        return new SDString(Integer.toHexString(i).toCharArray());
    }
    
    public static SDString toOctalString(int i){
        return new SDString(Integer.toOctalString(i).toCharArray());
    }
    
    @Override
    public SDString toSDString(){
        return new SDString(SDIntegerProxy.toString().toCharArray());
    }
    
    public static SDString toString(int i){
        return new SDString(Integer.toString(i).toCharArray());
    }
    
    public static SDString toString(int i, int radix){
        return new SDString(Integer.toString(i, radix).toCharArray());
    }
    
    public static SDInteger valueOf(SDString string){
        Integer tmp = Integer.valueOf(string.SDStringProxy);
        return new SDInteger(tmp);
    }
    
    public static SDInteger valueOf(SDString string, int radix){
        Integer tmp = Integer.valueOf(string.SDStringProxy, radix);
        return new SDInteger(tmp);
    }
    
    public static int highestOneBit(int i){
        return Integer.highestOneBit(i);
    }
    
    public static int lowestOneBit(int i){
        return Integer.lowestOneBit(i);
    }
    
    public static int numberOfLeadingZeros(int i){
        return Integer.numberOfLeadingZeros(i);
    }
    
    public static int numberOfTrailingZeros(int i){
        return Integer.numberOfTrailingZeros(i);
    }
    
    public static int bitCount(int i){
        return Integer.bitCount(i);
    }
    
    public static int rotateLeft(int i, int distance){
        return Integer.rotateLeft(i, distance);
    }
    
    public static int rotateRight(int i, int distance){
        return Integer.rotateRight(i, distance);
    }
    
    public static int reverseBytes(int i){
        return Integer.reverseBytes(i);
    }
    
    public static int reverse(int i){
        return Integer.reverse(i);
    }
    
    public static int signum(int i){
        return Integer.signum(i);
    }
    
    public static SDInteger valueOf(int i){
        return new SDInteger(Integer.valueOf(i));
    }
}
