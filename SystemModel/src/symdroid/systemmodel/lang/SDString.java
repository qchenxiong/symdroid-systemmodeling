package symdroid.systemmodel.lang;

import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;
import symdroid.systemmodel.io.SDSerializable;

/**
 * @date 2012-10-15
 * @author vincent
 */
public class SDString extends SDObject implements SDSerializable, SDComparable<SDString>, SDCharSequence{
    public static final char REPLACEMENT_CHAR = (char) 0XFFFD;
    
    //the proxy of SDString, use it to implements some complex functions
    public String SDStringProxy;
    
    private final char[] value;
    private final int offset;
    private final int count;
    private int hashCode;
    
    public SDString(){
        value = new char[0];
        offset = 0;
        count = 0;
    }
    
    public SDString(byte[] data){
        this(data,0,data.length);
    }
    
    public SDString(byte[] data, int offset, int byteCount){
        this(data,offset,byteCount,null);
    }
    
    public SDString(byte[] data, SDString charsetName){
        this(data,0,data.length,charsetName);
    }
    
    public SDString(byte[] data, int offset, int byteCount, SDString charsetName){
        String tmp = new String(charsetName.value);
        try {
            SDStringProxy = new String(data,offset,byteCount,tmp);
        } catch (UnsupportedEncodingException ex) {
            SDStringProxy = new String(data,offset,byteCount);
        } finally{
            this.value = SDStringProxy.toCharArray();
            this.offset = 0;
            this.count = byteCount;
        }
    }
    
    public SDString(char[] data){
        this(data,0,data.length);
    }
    
    public SDString(char[] data, int offset, int charCount){
        SDStringProxy = new String(data,offset,charCount);
        this.value = SDStringProxy.toCharArray();
        this.offset = 0;
        this.count = charCount;
    }
    
    public SDString(SDString toCopy){
        String tmp = new String(toCopy.value,toCopy.offset,toCopy.count);
        SDStringProxy = new String(tmp);
        this.value = SDStringProxy.toCharArray();
        this.offset = 0;
        this.count = value.length;
    }
    
    public SDString(SDStringBuilder stringBuilder){
        //TODO not implement correctly
        this.value = new char[0];
        this.offset = 0;
        this.count = 0;
    }

    @Override
    public int compareTo(SDString another) {
        String tmp = new String(another.value,another.offset,another.count);
        return SDStringProxy.compareTo(tmp);
    }
    
    public int compareToIgnoreCase(SDString another){
        String tmp = new String(another.value,another.offset,another.count);
        return SDStringProxy.compareToIgnoreCase(tmp);
    }
    
    public SDString concat(SDString another){
        if(another.count > 0 && this.count > 0){
            String tmp = new String(another.value, another.offset, another.count);
            String newString = SDStringProxy.concat(tmp);
            return new SDString(newString.toCharArray());
        }
        return this.count == 0 ? another : this;
    }
    
    public static SDString copyValueOf(char[] data){
        return new SDString(data);
    }
    
    public static SDString copyValueOf(char[] data, int start, int length){
        return new SDString(data, start,length);
    }
    
    public boolean endsWith(SDString suffix){
        String tmp = new String(suffix.value, suffix.offset, suffix.count);
        return SDStringProxy.endsWith(tmp);
    }
    
    public boolean equals(SDObject object){
        if(object instanceof SDString){
            SDString tmp = (SDString) object;
            return SDStringProxy.equals(tmp.SDStringProxy);
        }else{
            return false;
        }
    }
    
    public boolean equalsIgnoreCase(SDString another){
       return SDStringProxy.equalsIgnoreCase(another.SDStringProxy);
    }
    
    public void getBytes(int start, int end, byte[] data, int index){
        SDStringProxy.getBytes(start, end, data, index);
    }
    
    public byte[] getBytes(){
        return SDStringProxy.getBytes();
    }
    
    public byte[] getBytes(SDString charsetName){
        String tmp = new String(charsetName.value, charsetName.offset, charsetName.count);
        byte[] result = null;
        try {
            result = SDStringProxy.getBytes(tmp);
        } catch (UnsupportedEncodingException ex) {
            result = SDStringProxy.getBytes();
        }finally{
            return result;
        }
    }
    
    public void getChars(int start, int end, char[] buffer, int index){
        SDStringProxy.getChars(start, end, buffer, index);
    }
    
    public int hashCode(){
        return SDStringProxy.hashCode();
    }
    
    public int indexOf(int c){
        return SDStringProxy.indexOf(c);
    }
    
    public int indexOf(int c, int start){
        return SDStringProxy.indexOf(c, start);
    }
    
    public int indexOf(SDString sub){
        return SDStringProxy.indexOf(sub.SDStringProxy);
    }
    
    public int indexOf(SDString sub, int start){
        return SDStringProxy.indexOf(sub.SDStringProxy, start);
    }
    
    //TODO origin is native
    public SDString intern(){
        return this;
    }

    //TODO origin is native
    public boolean isEmpty(){
        return SDStringProxy.isEmpty();
    }
    
    public int lastIndexOf(int c){
        return SDStringProxy.lastIndexOf(c);
    }
    
    public int lastIndexOf(int c, int start){
        return SDStringProxy.lastIndexOf(c, start);
    }
    
    public int lastIndexOf(SDString sub){
        return SDStringProxy.lastIndexOf(sub.SDStringProxy);
    }
    
    public int lastIndexOf(SDString subString, int start){
        return SDStringProxy.lastIndexOf(subString.SDStringProxy, start);
    }
    
    public boolean regionMatches(int thisStart,  SDString another, int start, int length){
        return SDStringProxy.regionMatches(thisStart, another.SDStringProxy, start, length);
    }
    
    public boolean regionMatches(boolean ignoreCase, int thisStart, SDString another, int start, int length){
        return SDStringProxy.regionMatches(ignoreCase, thisStart, another.SDStringProxy, start, length);
    }
    
    public SDString replace(char oldChar, char newChar){
        String tmp = new String(value,offset,count);
        tmp = tmp.replace(oldChar, newChar);
        return new SDString(tmp.toCharArray());
    }
    
    public SDString replace(SDCharSequence target, SDCharSequence replacement){
        SDString trgt = target.toSDString();
        SDString rept = replacement.toSDString();
        String tmp = this.SDStringProxy.replace(trgt.SDStringProxy, rept.SDStringProxy);
        return new SDString(tmp.toCharArray());
    }
    
    public boolean startsWith(SDString prefix){
        return SDStringProxy.startsWith(prefix.SDStringProxy);
    }
    
    public boolean startsWith(SDString prefix, int start){
        return SDStringProxy.startsWith(prefix.SDStringProxy, start);
    }
    
    public SDString substring(int start){
        String tmp = SDStringProxy.substring(start);
        return new SDString(tmp.toCharArray());
    }
    
    public SDString substring(int start, int end){
        String tmp = SDStringProxy.substring(start, end);
        return new SDString(tmp.toCharArray());
    }
    
    public char[] toCharArray(){
        char[] buffer = new char[count];
        System.arraycopy(value, offset, buffer, 0, count);
        return buffer;
    }
    
    public SDString toLowerCase(){
        String tmp = SDStringProxy.toLowerCase();
        return new SDString(tmp.toCharArray());
    }
    
    public SDString toUpperCase(){
        String tmp = SDStringProxy.toUpperCase();
        return new SDString(tmp.toCharArray());
    }
    
    public SDString trim(){
        String tmp = SDStringProxy.trim();
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(char[] data){
        return new SDString(data);
    }
    
    public static SDString valueOf(char[] data, int start, int length){
        return new SDString(data, start, length);
    }
    
    public static SDString valueOf(char value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(double value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(float value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(int value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(long value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public static SDString valueOf(SDObject value){
        return value.toSDString();
    }
    
    public static SDString valueOf(boolean value){
        String tmp = String.valueOf(value);
        return new SDString(tmp.toCharArray());
    }
    
    public boolean contentEquals(SDCharSequence cs){
        SDString tmp = cs.toSDString();
        return SDStringProxy.contentEquals(tmp.SDStringProxy);
    }
    
    public boolean matches(SDString regularExpression){
        return Pattern.matches(regularExpression.SDStringProxy, SDStringProxy);
    }
    
    public SDString replaceAll(SDString regularExpression, SDString replacement){
        String result =  Pattern.compile(regularExpression.SDStringProxy).matcher(SDStringProxy).replaceAll(replacement.SDStringProxy);
        return new SDString(result.toCharArray());
    }
    
    public SDString replaceFirst(SDString regularExpression, SDString replacement){
        String result = Pattern.compile(regularExpression.SDStringProxy).matcher(SDStringProxy).replaceFirst(replacement.SDStringProxy);
        return new SDString(result.toCharArray());
    }
    
    public SDString[] split(SDString regularExpression){
        String[] tmp = SDStringProxy.split(regularExpression.SDStringProxy);
        SDString[] result = new SDString[tmp.length];
        for(int i = 0; i < result.length; i++){
            result[i] = new SDString(tmp[i].toCharArray());
        }
        return result;
    }
    
    public SDString[] split(SDString regularExpression, int limit){
        String[] tmp = SDStringProxy.split(regularExpression.SDStringProxy, limit);
        SDString[] result = new SDString[tmp.length];
        for(int i = 0; i < result.length; i++){
            result[i] = new SDString(tmp[i].toCharArray());
        }
        return result;
    }
    
    public boolean contains(SDCharSequence cs){
        return SDStringProxy.contains(cs.toSDString().SDStringProxy);
    }
    
    //TODO origin is native
    @Override
    public int length() {
        return count;
    }

    //TODO origin is native
    @Override
    public char charAt(int index) {
        return value[index];
    }

    @Override
    public SDCharSequence subSequence(int start, int end) {
        return substring(start, end);
    }

    @Override
    public SDString toSDString() {
        return this;
    }
    
}
