package symdroid.systemmodel.lang;

/**
 * @date 2012-10-15
 * @author vincent
 */
public class SDObject {
    
    public SDObject(){
    }
    
    protected SDObject clone(){
        return this;
    }
    
    public boolean equals(SDObject o){
        return this == o;
    }
    
    protected void finalize(){
    }
   
    //TODO
    public final native SDClass<?> getSDClass();
    
    //TODO
    public native int hashCode();
    
    //TODO
    public final native void SDNotify();    
    
    //TODO
    public final native void SDNotifyAll();
    
    public final void SDWait(){
        SDWait(0,0);
    }
    
    public final void SDWait(long millis){
        SDWait(millis,0);
    }
    
    //TODO
    public final native void SDWait(long millis, int nanos);
    
    //TODO
    public SDString toSDString(){
        return null;
    }
}
